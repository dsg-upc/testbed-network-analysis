#!/bin/sh -e

#set -x

. shell-db.d-utils/sdbu.sh

stderr() {
  echo "$@" >&2
  true
}

main() {
  cd "$(dirname "${0}")"

  PK_PATH="$HOME/.ssh/id_pedroupc"

  ssh-add "${PK_PATH}" 2>/dev/null || {
    stderr "ERROR: PK_PATH env var is ${PK_PATH}"
    false
  }
  net_file='db.d/network'
  ssh_cmd="uptime"
  #nodes="$(find "${nodes_dir}" -type f)"
  nodes="$(find -L "${net_file}" -maxdepth 1 -type d \
    | sort | grep -v "^${net_file}$" )"


  while IFS= read -r node_path <&9; do
    # retrieve variables of object node
    sdbu__read_obj "${node_path}"

    node_name="$(basename "$node_path")"
    cmd="ssh ${ssh_args} ${ssh_host} ${ssh_cmd}"
    [ -n "${UPTIME_DEBUG}" ] && printf "ssh command: ${cmd}\n\nnode_name: ${node_name}\nnode_path: ${node_path})\n"
    printf "____________________\n\n${node_name}\n"
    if ping -c1 -w1 "${ssh_host}" >/dev/null; then
      # DEBUG line
      #${cmd}
      output="$(${cmd})" || true
      echo "${output}"
    else
      stderr "WARNING: ${node_name} not responding to ping, skipped attempt to connect with ssh"
    fi
    #echo "${output}" > "${node_name}"
  done 9<<END
${nodes}
END
  true
}

main
