#!/bin/sh -eu

gen_template() {
  experiment="${1}"
  hosts="$(dirname $(grep -Ril netanalysis_name= db.d) | sort)"

  TEMPLATE="$(cat <<EOF
\documentclass{article}

\usepackage[table,xcdraw]{xcolor}

\begin{document}

\begin{table}[]
\begin{tabular}{|l|lllllll|}
\hline
& \multicolumn{7}{l|}{\\textbf{Bandwidth}} \\\\ \hline
\\textbf{}
$(
hosts_length="$(echo "${hosts}" | wc -w)"
counter=0
for s_host in ${hosts}; do
  counter="$((counter+1))"
  . "${s_host}/cfg.env"
  common_part='\cellcolor[HTML]{C0C0C0}\\textbf{'${netanalysis_name}'}'
  # last element is different
  if [ ${hosts_length} -ne ${counter} ]; then
    line='& \multicolumn{1}{l|}{'${common_part}'}'
  else
    line="& ${common_part}"
  fi
  printf "${line}\n"
done
)
\\\\ \hline
$(
for s_host in ${hosts}; do
  . "${s_host}/cfg.env"
  src_host="${netanalysis_name}"
  node='\cellcolor[HTML]{C0C0C0}\\textbf{'${netanalysis_name}'}'
  printf "${node} "

  # DEBUG
  #echo
  #echo "src host: ${netanalysis_name}"
  for d_host in ${hosts}; do
    . "${d_host}/cfg.env"
    dst_host="${netanalysis_name}"
    capture_path="${capture_target}/${src_host}/${dst_host}"
    result="$(cat ${capture_path}/${experiment})"
    ## DEBUG
    #echo
    #echo "${dst_host}: ${experiment_result}"
    cell='& \multicolumn{1}{l|}{'${result}'}'
    printf "${cell} "
  done
  printf ' \\\\ \hline\n'
done
)

\end{tabular}
\caption{todo between testbed nodes.}
\label{tab:testbedftodo}
\end{table}

\end{document}
EOF
)"
  thefile="output_template_${experiment}.tex"
  cat > "${thefile}" <<EOF
${TEMPLATE}
EOF
  pdflatex "${thefile}" >/dev/null
}

main() {
  if [ -n "${1}" ]; then
    capture_target="${1}"
  else
    #capture_target="db.d/output_collection/capture_2022-02-16_03-42-12"
    capture_target="db.d/output_collection/capture_2022-02-16_20-52-38"
  fi
  gen_template "avg_rtt"
  gen_template "hop_count"
  gen_template "throughput"
}

main "${@}"
