#!/bin/sh -e

. shell-db.d-utils/sdbu.sh

# because sometimes you want a lot of \n in your string
stderr() {
  printf -- "$@\n" >&2
}

safe_stderr() {
  # thanks https://unix.stackexchange.com/questions/519315/using-printf-to-print-variable-containing-percent-sign-results-in-bash-p/519316#519316
  printf "%s\n" "$@" >&2
}

EXP_DEBUG() {
  [ -n "${EXP_DEBUG}" ] && stderr "$@"
  true
}

# uses safe_stderr
SAFE_EXP_DEBUG() {
  [ -n "${EXP_DEBUG}" ] && safe_stderr "$@"
  true
}

# vars that are not args: ssh_args
ssh_cmd() {
  target_host="${1}"
  cmd_arg="${2}"
  if [ -z "${target_host}" ] || [ -z "${cmd_arg}" ]; then
    stderr 'ERROR: ssh_cmd: missing arguments'
    return 1
  fi
  # note: on localhost you would use eval to run a cmd
  # TODO evaluate args:
  #  -T : non interactive
  #  -n : avoid stdin (not needed in this case)
  cmd="ssh ${ssh_args} root@${target_host} ${cmd_arg}"
  EXP_DEBUG "${cmd}"

  output="$(${cmd})"
  SAFE_EXP_DEBUG "${output}"
  echo "${output}"
}

# uses ping
experiment_avg_rtt() {
  EXP_DEBUG 'avg_rtt'
  time="${time:-3}"
  cmd="ping -c ${time} ${dst_host_ip}"
  avg_rtt_raw="$(ssh_cmd "${src_host_ip}" "${cmd}")"
  echo "${avg_rtt_raw}" > "${base_path}/avg_rtt_raw"
  avg_rtt="$(echo "${avg_rtt_raw}" | grep 'rtt ' | cut -d'/' -f 5 )"
  echo "${avg_rtt}" > "${base_path}/avg_rtt"
}

# uses traceroute
experiment_hop_count() {
  EXP_DEBUG 'hop_count'
  cmd="traceroute -n ${dst_host_ip}"
  hop_count_raw="$(ssh_cmd "${src_host_ip}" "${cmd}")"
  echo "${hop_count_raw}" > "${base_path}/hop_count_raw"
  hop_count="$(echo "${hop_count_raw}" | tail -1 | awk '{ print $1 }')"
  echo "${hop_count}" > "${base_path}/hop_count"
}

# uses iperf3
experiment_throughput() {
  time="${time:-1}"
  EXP_DEBUG 'throughput'
  # case 1: throughput from client to server (cs)
  cmd="iperf3 -t ${time} -f m -c ${src_host_ip}"
  throughput_cs_raw="$(ssh_cmd "${dst_host_ip}" "${cmd}")"
  echo "${throughput_cs_raw}" > "${base_path}/throughput_cs_raw"
  # case 2: throughput from server to client (sc)
  cmd="iperf3 -t ${time} -f m -c ${src_host_ip} -R"
  throughput_sc_raw="$(ssh_cmd "${dst_host_ip}" "${cmd}")"
  echo "${throughput_sc_raw}" > "${base_path}/throughput_sc_raw"

  # conservative: looks like receiver always receive less
  tp1="$(echo "${throughput_cs_raw}" | grep receiver | awk '{print $7;}')"
  tp2="$(echo "${throughput_sc_raw}" | grep receiver | awk '{print $7;}')"
  echo "${tp1}/${tp2}" > "${base_path}/throughput"
}

run_experiments() {
  base_path="${capture_path}/${src_na_name}/${dst_na_name}"
  mkdir -p "${base_path}"

  experiment_avg_rtt
  experiment_hop_count
  experiment_throughput
}

start_iperf3() {
  # start if convenient
  cmd="pgrep iperf3"
  pgrep_output="$(ssh_cmd "${src_host_ip}" "${cmd}" || true)"
  #if [ -n "${pgrep_output}" ]; then
  # TODO DEBUG
  if [ -n "${pgrep_output}" ] && false; then
    EXP_DEBUG "iperf3 was already there"
    preserve_iperf3=true
  else
    cmd="iperf3 -s -D"
    ssh_cmd "${src_host_ip}" "${cmd}" >/dev/null
    EXP_DEBUG "started iperf3"
  fi
}

stop_iperf3() {
  # stop if convenient
  if [ -z "${preserve_iperf3}" ]; then
    cmd="pkill iperf3"
    # it's difficult to handle the iperf3 pid
    #cmd="kill ${iperf3_pid}"
    ssh_cmd "${src_host_ip}" "${cmd}"
  fi
}

# src https://stackoverflow.com/questions/5431909/returning-a-boolean-from-a-bash-function/43840545
check_ssh_host() {
  ssh_host="${1}"
  host_name="${2}"
  if nc -z -w 3 "${ssh_host}" 22 2>&1 >/dev/null \
    || [ "${bypass_check_ssh_host}" = "true" ]; then
    true
  else
    stderr "WARNING: ${src_host_name} not responding in the ssh port, skipped"
    false
  fi
}


# for experiments involving another host (dst_host)
proceed_with_dst_hosts() {
  local src_node="${1}"
  local hosts="${2}"

  start_iperf3

  #target_src_hosts="$(echo "${hosts}" | grep -v "${src_node}" | sort)"
  # running experiment against itself could be interesting (it's a benchmark to
  #   compare how heterogeneous are the machines)
  target_src_hosts="$(echo "${hosts}" | sort)"
  while IFS= read -r dst_node_path <&8; do
    dst_host_name="$(basename "$dst_node_path")"
    # we are interested in ${ssh_host} for dst_host
    sdbu__read_obj "${dst_node_path}"
    dst_na_name="${netanalysis_name}"
    dst_host_ip="${ssh_host}"

    EXP_DEBUG "- - - - - - - - -\n\n${src_host_name} -> ${dst_host_name}\n"
    if check_ssh_host "${ssh_host}" "${src_host_name}" >/dev/null; then
      run_experiments
    fi
  done 8<<END
${target_src_hosts}
END

  stop_iperf3

  true
}

proceed_on_src_hosts() {
  hosts="${1}"
  while IFS= read -r src_node_path <&9; do
    # retrieve variables of object node
    #   we are interested in ${ssh_host} for src_host_ip
    sdbu__read_obj "${src_node_path}"
    src_host_ip="${ssh_host}"
    src_host_name="$(basename "$src_node_path")"
    src_na_name="${netanalysis_name}"

    if check_ssh_host "${src_host_ip}" "${src_host_name}"; then
      EXP_DEBUG "_________________\n\n${src_host_name}\n"
      proceed_with_dst_hosts "${src_host_name}" "${hosts}"
    fi

  done 9<<END
${hosts}
END
}

main() {
  cd "$(dirname "${0}")"
  ts="$(date +'%Y-%m-%d_%H-%M-%S')"
  capture_path="${dbname}/output_collection/capture_${ts}"
  stderr "\n${capture_path}\n\n"

  PK_PATH="$HOME/.ssh/id_pedroupc"

  EXP_DEBUG="${EXP_DEBUG:-}"

  ssh-add "${PK_PATH}" 2>/dev/null || {
    stderr "ERROR: PK_PATH env var is ${PK_PATH}"
    false
  }
  net_file='db.d/network'

  if [ ! -d "${net_file}" ]; then
    echo 'no network directory detected, copying example network for a quick demo'
    cp -av "db.d/network.example" "db.d/network"
  fi

  # no, filter to certain hosts
  #hosts="$(find -L "${net_file}" -maxdepth 1 -type d \
  #  | sort | grep -v "^${net_file}$" )"
  hosts="$(dirname $(grep -Ril netanalysis_name= db.d | sort))"
  EXP_DEBUG "${hosts}"
  # DEBUG temp limit to 4
  #hosts="$(echo "${hosts}" | head -n 4)"
  # DEBUG, test with specific host
  #hosts='db.d/network/example1'
  #hosts='db.d/network/example2'

  proceed_on_src_hosts "${hosts}"
  true
}

main
