#!/bin/sh

stu__default_template='template'

stu__unsafe_stderr() {
  # https://stackoverflow.com/questions/2761723/what-is-the-difference-between-and-in-shell-scripts/2761739#2761739
  # shellcheck disable=SC2059
  printf -- "$*\n" >&2
}

# thanks https://unix.stackexchange.com/questions/519315/using-printf-to-print-variable-containing-percent-sign-results-in-bash-p/519316#519316
stu__stderr() {
  printf "%s\n" "$@" >&2
}

STU_DEBUG() {
  [ -n "${STU_DEBUG:-}" ] && stu__stderr "$@"
  true
}

# retrocompatibility
stu__process_file_template() {
  stu__template "$@"
  # TODO
  #stu__stderr "stu__proces_file_template is deprecated, use stu__template"
  #return 1
}

stu__template() {
  src_path="${1}"
  src_file="$(basename "${src_path}")"
  # if not specified and by default, template files are considered under certain default extension
  if [ -z "${tmplext}" ]; then
    tmplext="${stu__default_template}"
  fi

  if [ -z "${src_path}" ]; then
    stu__stderr "ERROR: missing src_path argument"
    return 1
  elif [ ! -f "${src_path}" ]; then
    stu__stderr "ERROR: ${src_file} template not found"
    return 1
  elif ! echo "${src_path}" | grep -E "\.${tmplext}$" >/dev/null; then
    stu__err_msg="$(cat <<EOF
ERROR: expected as input: ${src_file}.${tmplext}, but got: ${src_file}.
You can change tmplext env var to whatever you need. Now tmplext=${tmplext}
EOF
)"
    stu__stderr "${stu__err_msg}"
    return 1
  elif ! grep "TEMPLATE=" "${src_path}" >/dev/null; then
    stu__stderr "ERROR: [precheck] ${src_file} is not defining TEMPLATE var correctly"
    return 1
  fi
  dst_path="${src_path%.${tmplext}}"
  # special temp mode to find undefined variables
  if echo "$-" | grep -q u; then
    preserve_set_u='true'
  else
    preserve_set_u=
    set -u
  fi
  # preserve set -e if was set -> src https://stackoverflow.com/questions/65225164/posix-sh-check-test-value-of-builtin-set-option
  #   set +e needs to be set to make source `.` command work, at least in dash
  if echo "$-" | grep -q s; then
    preserve_set_e='true'
  else
    preserve_set_e=
    set +e
  fi
  # check that source is going to work before applying it
  # shellcheck disable=SC1090
  source_output="$(. "${src_path}" 2>&1)"
  STU_DEBUG "output: ${source_output}"
  if [ -n "${source_output}" ]; then
    stu__stderr "ERROR: template ${src_file} contains variable(s) not defined."
    stu__stderr "details:"
    stu__stderr "${source_output}"
    return 1
  fi
  # shellcheck disable=SC1090
  . "${src_path}" >/dev/null 2>&1

  # if not needed to preserve, revert modes
  if [ -z "${preserve_set_e}" ]; then
    set -e
  fi
  if [ -z "${preserve_set_u}" ]; then
    set +u
  fi

  #. "${src_path}"
  if [ -z "${TEMPLATE}" ]; then
    stu__stderr "ERROR: [postcheck] ${src_file} is not defining TEMPLATE var correctly"
    return 1
  fi
  # shellcheck disable=SC2153
  cat > "${dst_path}" <<EOF
$TEMPLATE
EOF
  # removes the generated template, useful when testing this code
  if [ "${DRY_TEST}" = true ]; then
    rm "${dst_path}"
  # removes the source
  elif [ "${RM_SRC}" = true ]; then
    rm "${src_path}"
  fi
  echo "${dst_path}"
}

stu__templates() {
  src_path="${1}"
  if [ -z "${src_path}" ]; then
    stu__stderr "ERROR: missing src_path argument"
    return 1
  fi
  if [ -z "${tmplext}" ]; then
    tmplext="${stu__default_template}"
  fi
  if [ ! -d "${src_path}" ]; then
    stu__stderr "ERROR: ${src_path} directory not found"
    return 1
  fi
  # find the templates
  templates="$(find "${src_path}" -name "*.${tmplext}" | sort)"
  if [ -n "${templates}" ]; then
    output=
    while IFS= read -r template <&9; do
      STU_DEBUG "processing template ${template}"
      output="$(stu__template "${template}")\n${output}" || {
        stu__stderr "ERROR processing ${template}"
        return 1
      }
    done 9<<EOF
${templates}
EOF
  fi
  if [ -n "${DRY_TEST}" ]; then
    # TODO I hope there is another way
    # shellcheck disable=SC2059
    printf "${output}"
  fi
  true
}
