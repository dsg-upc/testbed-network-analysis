#!/bin/sh -e

stderr() {
  # thanks https://unix.stackexchange.com/questions/519315/using-printf-to-print-variable-containing-percent-sign-results-in-bash-p/519316#519316
  printf "%s\n" "$@" >&2
}

main() {
  if [ -n "${1}" ]; then
    capture_target="${1}"
  else
    #capture_target='db.d/output_collection/capture_2022-02-16_20-52-38'
    capture_target='db.d/output_collection/capture_2022-02-16_20-51-29'
  fi

  hosts="$(dirname $(grep -Ril netanalysis_name= db.d) | sort)"

  for s_host in ${hosts}; do
    . "${s_host}/cfg.env"
    src_host="${netanalysis_name}"
    if [ ! -d "${s_host}" ]; then
      stderr "WARNING: ${s_host}: empty dir"
    fi

    for d_host in ${hosts}; do
      . "${d_host}/cfg.env"
      dst_host="${netanalysis_name}"
      capture_path="${capture_target}/${src_host}/${dst_host}"
      if [ ! -d "${capture_path}" ]; then
        stderr "WARNING: ${capture_path}: empty dir"
      fi
    done
  done
}

main "${@}"
